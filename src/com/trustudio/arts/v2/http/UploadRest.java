package com.trustudio.arts.v2.http;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.trustudio.arts.v2.util.Cons;
import com.trustudio.arts.v2.util.Debug;
import com.trustudio.arts.v2.util.StringUtil;

public class UploadRest extends RestConnection {

	///save?sitecode=BDGT002&pekerjaan=Existing+2G+%26+3G&dokumen=SSR&provider=TRI
	
	//save?sitecode=testing001&dokumen=TESTING&provider=TRI
	
	public String uploadPhoto(String[] dir) throws Exception{
		String message = null;
		
		try {
			String url 		= Cons.API_URL + String.format("/save?sitecode=%s&dokumen=%s&provider=%s", dir[3], dir[2], dir[1]).replace(" ", "%20");

			InputStream is	= connectPost(url, new ArrayList<NameValuePair>(0));
			
			if (is != null) {
				String response			= StringUtil.streamToString(is);
				//JSONObject jsonObj 		= (JSONObject) new JSONTokener(response).nextValue(); 
				
				Debug.i(response);
				
				is.close();
			}	
		} catch (Exception e) {
			throw e;
		} 
		
		return message;
	}
	
	public String uploadPhoto(String provider, String dokumen, String siteid, String no_tracking, String tracking_info) throws Exception{
		String message = null;
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		
        nameValuePairs.add(new BasicNameValuePair("tracking_info_" + no_tracking,	tracking_info));
		
		try {
			String url 		= Cons.API_URL + String.format("/save?sitecode=%s&dokumen=%s&provider=%s", siteid, dokumen, provider).replace(" ", "%20");

			InputStream is	= connectPost(url, nameValuePairs);
			
			if (is != null) {
				String response			= StringUtil.streamToString(is);
				//JSONObject jsonObj 		= (JSONObject) new JSONTokener(response).nextValue(); 
				
				Debug.i(response);
				
				is.close();
			}	
		} catch (Exception e) {
			throw e;
		} 
		
		return message;
	}
}
