package com.trustudio.arts.v2.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.ui.adapter.ProviderListAdapter;

public class ProviderFragment extends BaseFragment{
	private Context context;
	private ProviderListAdapter mAdapter;
	
	private OnProviderListListener mListener;
	
	public static ProviderFragment newInstance() {
		return new ProviderFragment();
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context 	= activity;
		mListener	= (OnProviderListListener) activity;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_provider, null);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		ListView mListView 	= (ListView) getView().findViewById(R.id.list_provider);
		
		mAdapter			= new ProviderListAdapter(context);
		
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				mListener.onProviderClickListener();
			}
		});
	}
	
	public interface OnProviderListListener{
		void onProviderClickListener();
	}
}
