package com.trustudio.arts.v2.ui;

import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.util.Cons;

import android.content.Intent;
import android.os.Bundle;

public class SplashScreenActivity extends BaseActivity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);
		
		showSplash();
	}
	
	private void showSplash() {
		Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int wait = 0;
                    
                    while(wait < Cons.SPLASHSCREEN_TIMEOUT){
                        sleep(100);
                        
                        wait += 100;                        
                    }
                } catch(InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                    
                    startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                }
            }
        };
        
        splashTread.start();
	}
}
