package com.trustudio.arts.v2.ui;

import java.io.File;
import java.io.FileOutputStream;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.ui.fragment.CandidateLocationMapFragment;
import com.trustudio.arts.v2.util.Cons;
import com.trustudio.arts.v2.util.Debug;
import com.trustudio.arts.v2.util.ImageHelper;
import com.trustudio.arts.v2.util.ImageUploader;
import com.trustudio.arts.v2.util.StorageManager;
import com.trustudio.arts.v2.util.ImageUploader.ImageUploadListener;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class CandidateLocationActivity extends BaseActivity {
	private CandidateLocationMapFragment mFragmentSupport;
	private ProgressDialog mProgress;
	
	private ImageView mPhotoNominal;
	
	private String siteID;
	private String dokumen;
	private String provider;
	
	String path;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_candidate_location);
		
		getSupportActionBar().setTitle(R.string.app_name);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setSupportProgressBarIndeterminateVisibility(false);
		
		Bundle bundle = getIntent().getExtras();
		siteID		  = bundle.getString("com.siteid");
		dokumen		  = bundle.getString("com.dokumen");
		provider	  = bundle.getString("com.provider");
		
		path = Environment.getExternalStorageDirectory() 
				+ "/Nexwave/" + provider + "/" + dokumen + "/" + siteID + "/Nominal/" + "photo_nominal_1.png";
		
		mPhotoNominal 		= (ImageView) findViewById(R.id.image);
		Button	mCandidate	= (Button) findViewById(R.id.candidate);
		
		mProgress			= new ProgressDialog(CandidateLocationActivity.this);
		
		if(exists(path)){
			Bitmap bmp = BitmapFactory.decodeFile(path);
			mPhotoNominal.setImageBitmap(bmp);
		}
		
		mPhotoNominal.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

				if(exists(path)){
					Intent intent = new Intent(CandidateLocationActivity.this, PhotoNominalViewActivity.class); 
					
					intent.putExtra("com.photo", path);
					
					startActivity(intent);
				}else{
					showToast("Foto tidak ada");
				}
				
				//startActivity(new Intent(CandidateLocationActivity.this, PhotoNominalViewActivity.class));
			}
		});
		
		mCandidate.setVisibility(View.GONE);
		mCandidate.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
			}
		});
		
		if(savedInstanceState == null){
			mFragmentSupport = CandidateLocationMapFragment.newInstance();
	
			getSupportFragmentManager().beginTransaction().add(R.id.map, mFragmentSupport).commit();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("Ambil Gambar Nominal")
		.setIcon(R.drawable.ic_menu_camera)
		.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				setPhotoNominal();
				return false;
			}
		})
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		
		menu.add("Upload Gambar Nominal")
		.setIcon(R.drawable.ic_menu_upload)
		.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				mProgress.setMessage("Uploading nominal photo...");
				mProgress.show(); 
				mProgress.setCanceledOnTouchOutside(false);
				
				uploadPhotoNominal();
				return false;
			}
		})
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	private void setPhotoNominal(){
		String[] items = { "Take From Camera", "Select From Gallery" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int pos) {
				Intent intent = null;
				switch (pos) {
				case 0:
					intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					intent.putExtra("outputX", 250);
					intent.putExtra("outputY", 250);
					intent.putExtra("return-data", true);
					break;
				case 1:
					intent = new Intent(Intent.ACTION_PICK);
					intent.setType("image/*");
					intent.putExtra("outputX", 250);
					intent.putExtra("outputY", 250);
					intent.putExtra("return-data", true);
					break;
				}
				
				int requestCode = 1;
				
				startActivityForResult(intent, requestCode);
				
				dialog.dismiss();
			}
		});
		builder.create().show();
	}
	
	public static boolean exists(String filepath){
		boolean retval = false;
		
		if(!filepath.toString().equals(null)){
			
			File f = new File(filepath);
			if(f.exists()){
				retval = true;
			}else{
				retval = false;
			}
		}
		
		return retval;
	}
	
	private void uploadPhotoNominal(){
		if(exists(path)){
			final ImageUploader imageUploader = new ImageUploader();
			
			imageUploader.setListener(new ImageUploadListener() {
				
				@Override
				public void onStart() {
					Debug.i(Cons.TAG, "Start");
				}
				
				@Override
				public void onProgress(int progress) {
					Debug.i(Cons.TAG, "Progress " + String.valueOf(progress));
				}
				
				@Override
				public void onFinish() {
					Debug.i(Cons.TAG, "Finishh");
					mProgress.dismiss();
					showDialogMessage("Success", "Photo berhasil di upload");
				}
				
				@Override
				public void onError(int errorCode) {
					 Debug.i(Cons.TAG, "Error " + String.valueOf(errorCode));
					showDialogMessage("OOPS", "Gagal upload photo");
					mProgress.dismiss();
				}
			});
			
			imageUploader.upload("foto_nominal_1", "foto_nominal_1", path, 
					Cons.API_URL + String.format("/save?sitecode=%s&dokumen=%s&provider=%s", siteID, dokumen, provider).replace(" ", "%20"));

		}else{
			mProgress.dismiss();
			showDialogMessage("OOPS", "Foto nominal tidak ada");
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			if (requestCode == 1) {
				try {
					Bitmap bmp;

					boolean isFromExtra = data.getExtras() == null ? false : true;
					if (isFromExtra) {
						bmp = (Bitmap) data.getExtras().get("data");
					} else {
						bmp = ImageHelper.getBitmapFromUri(this, data.getData());
						float degree = ImageHelper.rotationForImage(getApplicationContext(), data.getData());
						bmp = ImageHelper.rotate(bmp, (int) degree);
					}

					try {
						//new File(questionHelper.answer.getString(img.getTag().toString())).delete();
					} catch (Exception e) {
						Log.w("get photo path", e.toString());
					}
					
					File f = StorageManager.getImageFile("/" + provider + "/" + dokumen + "/" + siteID + "/Nominal/", "photo_nominal_1");
					FileOutputStream out = new FileOutputStream(f);
					bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
					mPhotoNominal.setImageBitmap(bmp);

					//questionHelper.inputData(img.getTag().toString(), f.getPath());
				} catch (Exception e) {
					e.printStackTrace();
					Log.e("get imageview", e.toString());
				}
			}
		}
	}
}
