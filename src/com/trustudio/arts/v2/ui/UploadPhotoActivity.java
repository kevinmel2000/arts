package com.trustudio.arts.v2.ui;

import java.io.File;
import java.util.ArrayList;

import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.http.UploadRest;
import com.trustudio.arts.v2.util.Cons;
import com.trustudio.arts.v2.util.Debug;
import com.trustudio.arts.v2.util.ImageUploader;
import com.trustudio.arts.v2.util.ImageUploader.ImageUploadListener;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class UploadPhotoActivity extends BaseActivity{
	private ProgressDialog mProgress;
	private UploadRest mRest;
	private String mSubDir;
	private String path ;
	
	private ArrayList<String> FilesInFolder = null; 
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_upload);
		
		getSupportActionBar().setTitle("Upload Photo");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setSupportProgressBarIndeterminateVisibility(false);
		
		Bundle bundle = getIntent().getExtras();
		mSubDir	= bundle.getString("com.subdir");
		
		path 	= Environment.getExternalStorageDirectory() + Cons.APP_DIR_OPEN + mSubDir;
		
		Debug.i(mSubDir);
		
		mRest		= new UploadRest();
		mProgress 	= new ProgressDialog(UploadPhotoActivity.this);
		
		ListView lv;
	    lv = (ListView)findViewById(R.id.list_image);
	    
	    try {
		    FilesInFolder = GetFiles(path);
		    
		    if(FilesInFolder == null){
		    	
		    }else{
		    	lv.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, FilesInFolder));
		    }
		} catch (Exception e) {
			showToast("Tidak ada file photo");
		}
	    
	    lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				String[] split = FilesInFolder.get(position).split("\\.");
				Debug.i(split[0]);
				try {
					doPost(split[0]);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public ArrayList<String> GetFiles(String DirectoryPath) {

	    ArrayList<String> MyFiles = new ArrayList<String>();
	    
		try {
			File f = new File(DirectoryPath);

		    f.mkdirs();
		    File[] files = f.listFiles();
		    if (files.length == 0)
		        return null;
		    else {
		        for (int i=0; i<files.length; i++){
		        	File file = files[i];
		            String filePath = file.getName();
		            if(filePath.endsWith(".png")) // Condition to check .jpg file extension
		            	MyFiles.add(filePath);
		        }
		            //MyFiles.add(files[i].getName());
		    }
		} catch (Exception e) {e.printStackTrace();
		}
	    

	    return MyFiles;
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	private void doPost(final String var) {
		mProgress.setMessage("Uploading photo...");
		mProgress.show(); 
		
		new Thread() {
			@Override
			public void run() {
				int what 		= 0;
				
				try {
					String[] dir = mSubDir.split("/");
					mRest.uploadPhoto(dir);
				} catch (Exception e) {
					what = 1; 
					e.printStackTrace();
				}
				
				mHandler.sendMessage(mHandler.obtainMessage(what, var));
			}
		}.start();
	}
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			final String var 	= (String) msg.obj;
			
			if(msg.what == 0){
				final ImageUploader imageUploader = new ImageUploader();
				
				imageUploader.setListener(new ImageUploadListener() {
					
					@Override
					public void onStart() {
						Debug.i(Cons.TAG, "Start");
					}
					
					@Override
					public void onProgress(int progress) {
						Debug.i(Cons.TAG, "Progress " + String.valueOf(progress));
					}
					
					@Override
					public void onFinish() {
						Debug.i(Cons.TAG, "Finishh");
						mProgress.dismiss();
						showDialogMessage("Success", "Photo berhasil di upload");
					}
					
					@Override
					public void onError(int errorCode) {
						 Debug.i(Cons.TAG, "Error " + String.valueOf(errorCode));
						showDialogMessage("OOPS", "Gagal upload photo");
					}
				});
				
				String[] dir = mSubDir.split("/");
				Debug.i(dir[0] + dir[1] + dir[2]);
				imageUploader.upload(var, var, path + "/" + var + ".png", 
						Cons.API_URL + "/save?sitecode=" + dir[3] + String.format("&dokumen=%s&provider=%s", dir[2], dir[1]).replace(" ", "%20"));
			}else{
				mProgress.dismiss();
				showDialogMessage("OOPS", "Gagal upload photo");
			}
		};
	};
}

