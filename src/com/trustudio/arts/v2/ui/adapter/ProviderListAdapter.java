package com.trustudio.arts.v2.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ProviderListAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private String[] mItem = { "TRI" };
	
	public ProviderListAdapter(Context context) {
		mInflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return mItem.length;
	}

	@Override
	public Object getItem(int position) {
		return mItem[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView	=  mInflater.inflate(android.R.layout.simple_list_item_1, null);
			
			holder = new ViewHolder();
			
			holder.mTextTitle		= (TextView) convertView.findViewById(android.R.id.text1);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.mTextTitle.setText(mItem[position]);
		
        return convertView;
	}
	
	static class ViewHolder{
		TextView mTextTitle;
	}
}
