package com.trustudio.arts.v2.ui.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.model.TrackingModel;

public class TrackingUploadAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private ArrayList<TrackingModel> mList;
	
	public TrackingUploadAdapter(Context context) {
		mInflater = LayoutInflater.from(context);
	}
	
	public void setData(ArrayList<TrackingModel> mList){
		this.mList = mList;
	}
	
	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView	=  mInflater.inflate(android.R.layout.simple_list_item_1, null);
			
			holder = new ViewHolder();
			
			holder.mTextTitle		= (TextView) convertView.findViewById(android.R.id.text1);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.mTextTitle.setTextColor(R.color.text_grey);
		holder.mTextTitle.setText(mList.get(position).caption);
		
        return convertView;
	}
	
	static class ViewHolder{
		TextView mTextTitle;
	}
}
