package com.trustudio.arts.v2.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.util.Cons;
import com.trustudio.arts.v2.util.Debug;

public class BaseActivity extends SherlockFragmentActivity {
	protected SQLiteDatabase mSqLite;
	protected SharedPreferences mSharedPref;
	
	private boolean mIsDbOpen = false;
	private boolean mEnableDb = false;

	private String mTag = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		super.onCreate(savedInstanceState);
		
		setSupportProgressBarIndeterminateVisibility(false);

		mSharedPref		= getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);
		
		int currVersion	= mSharedPref.getInt(Cons.DBVER_KEY, 0);
		
		if (Cons.DB_VERSION > currVersion) {
			initDb();
		}		
		
		enableDatabase();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (mEnableDb && !mIsDbOpen) {
			openDatabase();
		}
	}
	
	@Override
	public void onPause() {
		if (mEnableDb && mIsDbOpen) {
			closeDatabase();
		}
		
		super.onPause();
	}
	
	public void back() {
		setResult(RESULT_OK);
        finish();
	}
	
	public SQLiteDatabase getDatabase() {
    	return mSqLite;
    }
	
	public SharedPreferences getSharedPreferences() {
		return mSharedPref;
	}
	
	public void logout() {
		Editor editor = mSharedPref.edit();
		
		
		editor.commit();
	}
	
	public void confirmExit() {
    	Builder builder = new AlertDialog.Builder(this);

		builder.setMessage(getString(R.string.text_confirm_exit))	  	  	   
	  	    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
	  	  		@Override
	  	  		public void onClick(DialogInterface dialog, int which) {
	  	  			dialog.dismiss();
	  	  		}
	  	    })
	  	    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
	  	  		@Override
	  	  		public void onClick(DialogInterface dialog, int which) {
					logout();
					
					finish();
	  	  		}
	  	    });
	  	  	
		builder.create().show();
    }
	
	public void showInfo(String title, String info, final boolean back) {
		Builder builder = new AlertDialog.Builder(this);

		if (!title.equals("")) {
			builder.setTitle(title);
		}
		
		builder.setMessage(info)	  	  	   	  	    
		  	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		  	  		@Override
		  	  		public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						
						if (back) {
							back();
						}
		  	  		}
		  	    });
		  	  	
		builder.create().show();
	}
	
	public void showInfo(String title, String info) {
		showInfo(title, info, false);
	}
	
	public void showInfo(String info) {
		showInfo("", info, false);
	}
	
	public void showError(String error) {
		showInfo(getString(R.string.text_error), error);
	}
	
	public void showSuccess(String message) {
		showInfo(getString(R.string.text_success), message);
	}
	
	public void showSuccess(String message, boolean back) {
		showInfo(getString(R.string.text_success), message, back);
	}
	
	public void showDialogMessage(String title, String message) {
		final AlertDialog alertDialog;
	    alertDialog = new AlertDialog.Builder(this).create();
	    alertDialog.setCanceledOnTouchOutside(false);
	    alertDialog.setTitle(title);
	    alertDialog.setMessage(message);
	    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
	            new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int which) {
	                	alertDialog.dismiss();
	                }
	            });
	    alertDialog.show();
	}
	
	public void showDialogMessage(String title, String message, final Activity activity) {
		Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(title);
		builder.setMessage(message)	  	  	   	  	    
		  	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		  	  		@Override
		  	  		public void onClick(DialogInterface dialog, int which) {
						activity.finish();
		  	  		}
		  	    });
		  	  	
		builder.create().show();
	}
	

	public void showDialogMessageSuccess(String title, String message, final Activity activitys, final Class clas, final String siteID, final String subdir) {
		final AlertDialog alertDialog;
	    alertDialog = new AlertDialog.Builder(this).create();
	    alertDialog.setCanceledOnTouchOutside(false);
	    alertDialog.setTitle(title);
	    alertDialog.setMessage(message);
	    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
	            new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int which) {
	                	alertDialog.dismiss();
	                	activitys.finish();
	                	
	                	Intent intent = new Intent(activitys, clas);
	                	
	                	intent.putExtra("com.siteid", siteID);
	                	intent.putExtra("com.subdir", subdir);
	                	
	                	startActivity(intent);
	                }
	            });
	    alertDialog.show();
	}
	
	public String getTag() {
		return mTag;
	}
	
	protected void enableDatabase() {
		mEnableDb = true;
		
		openDatabase();
	}
	
	private void openDatabase() {
		if (mIsDbOpen) {
			Debug.i(Cons.TAG, "Database already open");
			return;
		}
		
		String db = Cons.DBPATH + Cons.DBNAME;
		
		try {
			mSqLite 	= SQLiteDatabase.openDatabase(db, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			mIsDbOpen	= mSqLite.isOpen();
			
			Debug.i(Cons.TAG, "Database open");			
		} catch (SQLiteException e) {
			Debug.e(Cons.TAG, "Can not open database " + db, e);
		}
	}
	
	private void initDb() {
		String database 		= Cons.DBPATH + Cons.DBNAME;
		
		SharedPreferences spref = getSharedPreferences(Cons.PRIVATE_PREF, Context.MODE_PRIVATE);					
		int currVersion			= spref.getInt(Cons.DBVER_KEY, 0);

		try {
			Debug.i(Cons.TAG, "Current database version is " + String.valueOf(currVersion));
			
			if (Cons.DB_VERSION > currVersion) {
				File databaseFile  = new File(database);
			
				if (databaseFile.exists()) {
					Debug.i(Cons.TAG, "Deleting current database " + Cons.DBNAME);
					
					databaseFile.delete();
				}
				
				InputStream is	= getResources().getAssets().open(Cons.DBNAME);
				OutputStream os = new FileOutputStream(database);
				
				byte[] buffer	= new byte[1024];
				int length;
				
				Debug.i(Cons.TAG, "Start copying new database " + database + " version " + String.valueOf(Cons.DB_VERSION));
				
				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}
				
				os.flush();
				os.close();
				is.close();
				
				Editor editor = spref.edit();
				
				editor.putInt(Cons.DBVER_KEY, Cons.DB_VERSION);				
				editor.commit();
			} 
		} catch (SecurityException ex) {
			Debug.e(Cons.TAG, "Failed to delete current database " + Cons.DBNAME, ex);
		} catch (IOException ex) {
			Debug.e(Cons.TAG, "Failed to copy new database " + Cons.DBNAME + " version " + String.valueOf(Cons.DB_VERSION), ex);
		}
	}
	
	private void closeDatabase() {
		if (!mIsDbOpen) return;
		
		mSqLite.close();
		
		mIsDbOpen = false;
		
		Debug.i(Cons.TAG, "Database closed");
	}
	
	public void showToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}
	
	public void showToast(int message) {
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	public void setTitles(String title){
		getSupportActionBar().setTitle(title);
	}
	
	public void setTitles(int titleId){
		getSupportActionBar().setTitle(titleId);
	}
	
	public String getTitles(int position){
        String[] item = getResources().getStringArray(R.array.ConfigTitle);

        return item[position];
	}
	
	public int lengthTitles(){
		String[] item = getResources().getStringArray(R.array.ConfigTitle);

		return item.length;
	}
}