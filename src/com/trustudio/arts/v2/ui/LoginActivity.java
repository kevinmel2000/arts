package com.trustudio.arts.v2.ui;

import com.trustudio.arts.v2.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class LoginActivity extends BaseActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		Button bLogin	= (Button) findViewById(R.id.login);
		
		bLogin.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				finish();
                
                startActivity(new Intent(LoginActivity.this, ConfigFormActivity.class));
			}
		});
	}
}
