package com.trustudio.arts.v2.db;

import java.util.ArrayList;

import com.trustudio.arts.v2.model.TrackingModel;
import com.trustudio.arts.v2.util.Debug;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MasterDb extends Database{

	public MasterDb(SQLiteDatabase sqlite) {
		super(sqlite);
		// TODO Auto-generated constructor stub
	}
	
	public void insert(String provider, String dokumen, String siteid, String caption, String location, String path, String no_tracking) {
		if (mSqLite == null) {
			return;
		}
		
		ContentValues values = new ContentValues();
		
		values.put("provider", 	provider);
		values.put("dokumen",  	dokumen);
		values.put("siteid",   	siteid);
		values.put("caption",  	caption);
		values.put("location", 	location);
		values.put("path",   	path);
		values.put("no_tracking",   no_tracking);
		
		Debug.i("Insert caption: " + caption);
			
		mSqLite.insert("tracking", null, values);				
	}
	
	public ArrayList<TrackingModel> getTrackingList(String provider, String dokumen, String siteid) {
		ArrayList<TrackingModel> list = null;
		
		if (mSqLite == null) {
			return list;
		}
		
		String sql = "SELECT * FROM tracking WHERE provider = '" + provider + "' AND dokumen = '" + dokumen + "' AND siteid = '" + siteid + "'";
		
		Cursor c = mSqLite.rawQuery(sql, null);
	
		if (c != null) {
			if (c.moveToFirst()) {
				list = new ArrayList<TrackingModel>();				
				
				while (c.isAfterLast()  == false) {
					TrackingModel menu 		= new TrackingModel();
					
					menu.id			= c.getInt(c.getColumnIndex("id"));
					menu.provider	= c.getString(c.getColumnIndex("provider"));
					menu.dokumen	= c.getString(c.getColumnIndex("dokumen"));
					menu.siteid		= c.getString(c.getColumnIndex("siteid"));
					menu.caption	= c.getString(c.getColumnIndex("caption"));
					menu.location	= c.getString(c.getColumnIndex("location"));
					menu.path		= c.getString(c.getColumnIndex("path"));
					menu.no_tracking= c.getString(c.getColumnIndex("no_tracking"));
					
					list.add(menu);
					
					c.moveToNext();
				}
			}
			
			c.close();
		}
		
		return list;
	}
}
