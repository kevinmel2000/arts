package com.trustudio.arts.v2.util;

public class URLCons {

	public static final String UTF_8 = "utf-8";

	public final static String BASE_URL = "http://3trust.com/arts_beta/web";
	//
	//http://www.3trust.com/nexwave/tower2/web/

	public final static String ALL_QUESTION = "pertanyaan/api/json";
	//PertanyaanDokumen/api/json
	
	public final static String TEMPLATE_DOC = "TemplateDokumen/api/json";

	public final static String LOGIN = "User/api/json?q=admin";
	public final static String PROVIDER_API = "Provider/api/json";
	public final static String JENIS_DOKUMEN = "JenisDokumen/api/json?ProviderId=%d";
	public final static String JENIS_PEKERJAAN = "JenisPekerjaan/api/json";
	public final static String SITE_BY_SITE_CODE = "Site/api/json?SiteCode=%s";
	public final static String SITE_BY_PROVIDER_ID = "Site/api/json?ProviderId=%d";
	public final static String GET_TEMPLATE = "/dokumen/api/pertanyaan?sitecode=%1s&dokumen=%2s&provider=%3s";
	//dokumen/api/json?sitecode=%1s&pekerjaan=%2s&dokumen=%3s&provider=%4s
			
	public final static String GET_QUESTION = "/dokumen/api/pertanyaan?sitecode=%1s&dokumen=%2s&provider=%3s";
	//PertanyaanDokumen/api/json?TemplateDokumenId=

	/* Post data */
	public final static String POST_DATA = "/dokumen/api/save?sitecode=%s&dokumen=%s&provider=%s";
	//dokumen/api/save?sitecode=%s&pekerjaan=%s&dokumen=%s&provider=%s

}
