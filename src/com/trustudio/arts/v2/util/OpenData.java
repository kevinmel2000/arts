package com.trustudio.arts.v2.util;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;

public class OpenData {

	private static final String USER_DATA = "USER_DATA";
	private static final String PREF_QUESTION = "PREF_QUEST";
	private static final String PREF_TEMPLATE = "PREF_TEMPLATE";
	private static final String PREF_TEMPLATE_ID = "PREF_TEMPLATE_ID";
	private static final String PREF_QUESTION_ANSWER = "PREF_QUESTION_ANSWER";

	/* form config preference */
	private static final String PREF_PROVIDER = "PREF_PROVIDER";
	private static final String PREF_SITE_CODE = "PREF_SITE_CODE";
	private static final String PREF_SITE_CODE2 = "PREF_SITE_CODE2";
	private static final String PREF_DOCUMENT = "PREF_DOCUMENT";
	private static final String PREF_JOB = "PREF_JOB";

	/* save tempalte data */
	public static void saveTemplate(ContextWrapper ctx, String content, int templateID) {
		SharedPreferences pref = ctx.getSharedPreferences(PREF_TEMPLATE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(String.valueOf(templateID), content);
		editor.commit();
	}

	/* get template data */
	public static String getTemplate(ContextWrapper ctx, int templateID) {
		SharedPreferences pref = ctx.getSharedPreferences(PREF_TEMPLATE, Context.MODE_PRIVATE);
		return pref.getString(String.valueOf(templateID), null);
	}

	public static void clearTemplate(ContextWrapper ctx) {
		SharedPreferences pref = ctx.getSharedPreferences(PREF_TEMPLATE, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.clear();
		editor.commit();
	}

	public static void saveQuestionData(ContextWrapper ctx, String content, int templateDocId) {
		SharedPreferences pref = ctx.getSharedPreferences(PREF_QUESTION, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString("data" + templateDocId, content);
		editor.putBoolean(String.valueOf(templateDocId), false);
		editor.commit();
	}

	public static String getQuestionData(ContextWrapper ctx, int tempalteDocId) {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences pref = ctx.getSharedPreferences(PREF_QUESTION, mode);
		return pref.getString("data" + tempalteDocId, "");
	}

	/* save template id with identifier link */
	public static void saveTemplateID(ContextWrapper wrapper, int templateID, String iden) {
		wrapper.getSharedPreferences(PREF_TEMPLATE_ID, Context.MODE_PRIVATE).edit().putInt(iden, templateID)
				.commit();
	}
	
	/* get template id with identifier link */
	public static int getTemplateID(ContextWrapper ctx, String iden) {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences pref = ctx.getSharedPreferences(PREF_TEMPLATE_ID, mode);
		return pref.getInt(iden, -1);
	}

	public static boolean isQuestionDataEmpty(ContextWrapper ctx, int templateDocId) {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences pref = ctx.getSharedPreferences(PREF_QUESTION, mode);
		return pref.getBoolean(String.valueOf(templateDocId), true);
	}

	public static void clearQuestionData(ContextWrapper ctx) {
		int mode = Activity.MODE_PRIVATE;
		SharedPreferences pref = ctx.getSharedPreferences(PREF_QUESTION, mode);
		SharedPreferences.Editor editor = pref.edit();
		editor.clear();
		editor.commit();
	}

	public static void saveUserData(ContextWrapper ctx, String value, String password) {
		SharedPreferences.Editor editor = ctx.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE).edit();
		editor.putString("username", value);
		editor.putString("password", password).commit();
	}

	public static String[] getUserData(ContextWrapper wrapper) {
		SharedPreferences pref = wrapper.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
		String[] data = new String[2];
		data[0] = pref.getString("username", null);
		data[1] = pref.getString("password", null);

		return data;
	}

	public static void clearUserData(ContextWrapper wrapper) {
		wrapper.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE).edit().clear().commit();
	}

	public static void saveAnswerData(ContextWrapper wrapper, int templateID, String value) {
		wrapper.getSharedPreferences(PREF_QUESTION_ANSWER, Context.MODE_PRIVATE).edit()
				.putString(String.valueOf(templateID), value).commit();
	}

	public static String getAnswerData(ContextWrapper wrapper, int templateID) {
		return wrapper.getSharedPreferences(PREF_QUESTION_ANSWER, Context.MODE_PRIVATE).getString(
				String.valueOf(templateID), null);
	}

	/* ======================= FORM CONFIG DATA =========================== */

	public static void saveProviderData(ContextWrapper wrapper, String value) {
		wrapper.getSharedPreferences(PREF_PROVIDER, Context.MODE_PRIVATE).edit()
				.putString(PREF_PROVIDER, value).commit();
	}

	public static String getProviderData(ContextWrapper wrapper) {
		return wrapper.getSharedPreferences(PREF_PROVIDER, Context.MODE_PRIVATE).getString(PREF_PROVIDER,
				null);
	}

	public static void saveSiteCodeData(ContextWrapper wrapper, String provider, String value) {
		wrapper.getSharedPreferences(PREF_SITE_CODE, Context.MODE_PRIVATE).edit().putString(provider, value)
				.commit();
	}
	
	/* Tambahan sigue */
	
	public static void saveSiteCodeData(ContextWrapper wrapper, String provider, String dokumen, String value) {
		SharedPreferences pref = wrapper.getSharedPreferences(PREF_SITE_CODE2 + dokumen, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(provider, value);
		editor.commit();
	}

	public static String getSiteCodeData(ContextWrapper wrapper, String provider) {
		return wrapper.getSharedPreferences(PREF_SITE_CODE, Context.MODE_PRIVATE).getString(provider, null);
	}
	
	/* Tambahan sigue */
	public static String getSiteCodeData(ContextWrapper wrapper, String provider, String dokumen) {
		return wrapper.getSharedPreferences(PREF_SITE_CODE2 + dokumen, Context.MODE_PRIVATE).getString(provider, null);
	}

	public static void saveDocumentData(ContextWrapper wrapper, String provider, String value) {
		wrapper.getSharedPreferences(PREF_DOCUMENT, Context.MODE_PRIVATE).edit().putString(provider, value)
				.commit();
	}

	public static String getDocumentData(ContextWrapper wrapper, String provider) {
		return wrapper.getSharedPreferences(PREF_DOCUMENT, Context.MODE_PRIVATE).getString(provider, null);
	}

	public static void saveJobData(ContextWrapper wrapper, String value) {
		wrapper.getSharedPreferences(PREF_JOB, Context.MODE_PRIVATE).edit().putString(PREF_JOB, value)
				.commit();
	}

	public static String getJobData(ContextWrapper wrapper) {
		return wrapper.getSharedPreferences(PREF_JOB, Context.MODE_PRIVATE).getString(PREF_JOB, null);
	}

}
