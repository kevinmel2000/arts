/**
 *  Lorensius W. L. T <lorenz@londatiga.net>
 *  
 *  Last update by Fuad Hamidan <vootsugu@gmail.com>: September 28, 2012 18:12
 */

package com.trustudio.arts.v2.util;

import java.net.HttpURLConnection;
import java.net.URL;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import android.os.Handler;
import android.os.Message;

public class ImageUploader {
	private ImageUploadListener mListener;
	
	public static final int INVALID_FILE = 1;
	public static final int SERVER_ERROR = 2;
	
	public ImageUploader() { }
	
	public ImageUploader(ImageUploadListener listener) {
		mListener	= listener;
	}
	
	public void setListener(ImageUploadListener listener) {
		mListener	= listener;
	}
	
	public void upload(final String var, final String name, final String filePath, final String serverUrl) {
		new Thread() {
			@Override
			public void run() {
				doUpload(var, name, filePath, serverUrl);
			}
		}.start();
	}
	
	private void doUpload(String var, String name, String filePath, String serverUrl) {
		
		HttpURLConnection connection 	= null;
		DataOutputStream outputStream 	= null;

		String lineEnd 		= "\r\n";
		String twoHyphens 	= "--";
		String boundary 	=  "*****";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024;

		File file = new File(filePath);
		
		if (!file.exists()) {
			mHandler.sendMessage(mHandler.obtainMessage(INVALID_FILE, -1, 0));
			
			return;
		}
		
		try {
			FileInputStream fileInputStream = new FileInputStream(file);

			URL url 	= new URL(serverUrl);
			connection 	= (HttpURLConnection) url.openConnection();
			
			connection.setDoInput(true);
            connection.setDoOutput(true);
			connection.setUseCaches(false);

			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			outputStream = new DataOutputStream( connection.getOutputStream() );
			
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream.writeBytes("Content-Disposition: form-data; name=\"" + var + "\";filename=\"" + name + "\"" + lineEnd);
			outputStream.writeBytes(lineEnd);

			bytesAvailable 	= fileInputStream.available();
			bufferSize 		= Math.min(bytesAvailable, maxBufferSize);
			buffer 			= new byte[bufferSize];

			// 	Read file
			bytesRead 		= fileInputStream.read(buffer, 0, bufferSize);
			int total		= bytesAvailable;
			
			mHandler.sendMessage(mHandler.obtainMessage(0, -2, 0));
			
			while (bytesRead > 0) {
				outputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				android.util.Log.d(Cons.TAG, "Upload response: " + String.valueOf(bytesAvailable));
				
				mHandler.sendMessage(mHandler.obtainMessage(0, (int) ((total-bytesAvailable)*100)/total, 0));
			}

			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			//Responses from the server (code and message)
			//int serverResponseCode = connection.getResponseCode();
			android.util.Log.d(Cons.TAG, "Upload response: " + connection.getResponseCode());
			String serverResponse =	convertStreamToString(connection.getInputStream());
			
			android.util.Log.d(Cons.TAG, "Upload response: " + serverResponse);
			
			fileInputStream.close();
			outputStream.flush();
			outputStream.close();
			
			mHandler.sendMessage(mHandler.obtainMessage(0, -1, 0));
		} catch (Exception ex) {
			ex.printStackTrace();
			mHandler.sendMessage(mHandler.obtainMessage(SERVER_ERROR, -1, 0));
		}
	}
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 0) {
				if (msg.arg1 == -1)
					mListener.onFinish();
				else if (msg.arg1 == -2) 
					mListener.onStart();
				else
					mListener.onProgress(msg.arg1);
			} else {
				mListener.onError(msg.what);
			}
		}
	};
	
	private String convertStreamToString(InputStream is) throws Exception {
	    BufferedReader reader 	= new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb 		= new StringBuilder();
	    String line 			= null;
	    
	    while ((line = reader.readLine()) != null) {
	    	sb.append(line + "\n");
	    }
	    
	    is.close();
	    
	    return sb.toString();
	}
	 
	public interface ImageUploadListener {
		public abstract void onStart();
		public abstract void onProgress(int progress);
		public abstract void onFinish();
		public abstract void onError(int errorCode);
	}
}