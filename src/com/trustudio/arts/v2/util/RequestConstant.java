package com.trustudio.arts.v2.util;

public class RequestConstant {

	public final static int REQUEST_SIGNATURE = 1;
	public final static int REQUEST_PHOTO = 2;
	public final static int REQUEST_ATTACHMENT = 3;
	public final static int REQUEST_CODE_SCAN = 4;

	public final static int RESULT_SIGNATURE = 0x69;

}
