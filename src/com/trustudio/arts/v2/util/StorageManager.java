package com.trustudio.arts.v2.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.os.Environment;
import android.util.Log;

public class StorageManager {

	public final static String FOLDER = "/Nexwave";

	public static boolean isSDCARDMounted() {
		String status = Environment.getExternalStorageState();

		if (status.equals(Environment.MEDIA_MOUNTED))
			return true;
		return false;
	}

	public static void copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
	}

	public static File getSignatureFile(String subDirPath) {
		File dir = new File(Environment.getExternalStorageDirectory() + FOLDER + subDirPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File img = new File(dir, "signature" + System.currentTimeMillis() + ".png");

		try {
			if (img.exists()) {
				img.delete();
				img.createNewFile();
			} else {
				img.createNewFile();
			}
		} catch (IOException e) {
			Log.e(img.getAbsolutePath(), e.toString());
		}
		return img;
	}

	public static File getImageFile(String subDirPath) {
		File dir = new File(Environment.getExternalStorageDirectory() + FOLDER + subDirPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File img = new File(dir, "photo" + System.currentTimeMillis() + ".png");

		try {
			if (img.exists()) {
				img.delete();
				img.createNewFile();
			} else {
				img.createNewFile();
			}
		} catch (IOException e) {
			Log.e(img.getAbsolutePath(), e.toString());
		}
		return img;
	}
	
	public static File getImageFile2(String subDirPath, String var) {
		File dir = new File(Environment.getExternalStorageDirectory() + FOLDER + subDirPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File img = new File(dir, var + ".png");

		try {
			if (img.exists()) {
				img.delete();
				img.createNewFile();
			} else {
				img.createNewFile();
			}
		} catch (IOException e) {
			Log.e(img.getAbsolutePath(), e.toString());
		}
		return img;
	}
	
	public static File getImageFile(String subDirPath, String siteID) {
		File dir = new File(Environment.getExternalStorageDirectory() + FOLDER + subDirPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File img = new File(dir, siteID + ".png");

		try {
			if (img.exists()) {
				img.delete();
				img.createNewFile();
			} else {
				img.createNewFile();
			}
		} catch (IOException e) {
			Log.e(img.getAbsolutePath(), e.toString());
		}
		return img;
	}

	public static File getAnswerFile(String subdirPath) {
		File dir = new File(Environment.getExternalStorageDirectory() + FOLDER + subdirPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		File jsonFile = new File(dir, "answer.json");

		try {
			if (!jsonFile.exists())
				jsonFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonFile;
	}

	public static String getPath(String subDir) {
		return Environment.getExternalStorageDirectory() + FOLDER + subDir;
	}

	public static String createPath(String[] dirs) {

		StringBuffer folder = new StringBuffer();

		for (String dir : dirs) {
			folder.append("/" + dir);
		}

		File dir = new File(Environment.getExternalStorageDirectory() + FOLDER + folder);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		return folder.toString() + "/";
	}
}
