package com.trustudio.arts.v2.util;

public class QuestionCons {

	public final static String ID = "Id";
	public final static String TEMPLATE_DOC_ID = "TemplateDokumenId";
	public final static String TEMPLATE_DOC_ID_STRING = "TemplateDokumenId_toString";
	public final static String GROUP_QUESTION_ID = "GroupPertanyaanId";
	public final static String GROUP_QUESTION_ID_STRING = "GroupPertanyaanId_toString";
	public final static String QUESTION = "Question";
	public final static String HINT = "Hint";
	public final static String ANSWER_TYPE_ID = "AnswerTypeId";
	public final static String ANSWER_TYPE_ID_STRING = "AnswerTypeId_toString";
	public final static String OPT_VAL = "OptVal";
	public final static String VARIABLE = "VarName";
	public final static String GLOBAL_FILED_NAME = "GlobalFieldName";
	public final static String PARENT_ID = "ParentId";
	public final static String PARENT_ANSWER = "ParentAnswer";
	public final static String URUTAN = "Urutan";
	public final static String REQ_TYPE_ID = "RequiredTypeId";
	public final static String INFO = "Info";
	public final static String CONDITION = "VisibleConditions";
	public final static String TRIGGER_TO = "TriggerTo";
	public final static String ANSWER = "Answer1";

	/** inside condition **/
	public final static String OPERATOR = "Operator";
	public final static String KONDISI = "Kondisi";

	public final static int TYPE_GPS_LOC = 1;
	public final static int TYPE_TEXT = 2;
	public final static int TYPE_YES_NO = 3;
	public final static int TYPE_OK_NOK = 4;
	public final static int TYPE_OK_NOK_NA = 5;
	public final static int TYPE_MULTI_RADIO = 6;
	public final static int TYPE_MULTI_SELECT = 7;
	public final static int TYPE_PHOTO = 8;
	public final static int TYPE_ATTACHMENT = 9;
	public final static int TYPE_DATE = 10;
	public final static int TYPE_TIME = 11;
	public final static int TYPE_DATE_TIME = 12;
	public final static int TYPE_INTEGER = 13;
	public final static int TYPE_DECIMAL = 14;
	public final static int TYPE_SIGNATURE = 15;
	public final static int TYPE_PASSWORD = 16;
	public final static int TYPE_CODE_SCAN = 17;
	public final static int TYPE_GROUP_TEXT = 19;

}
