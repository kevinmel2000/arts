package com.trustudio.arts.v2.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.trustudio.arts.v2.R;
import com.trustudio.arts.v2.model.QuestionData;
import com.trustudio.arts.v2.model.QuestionData.OnAnswerTriggerListener;
import com.trustudio.arts.v2.model.ViewDataHolder;
import com.trustudio.arts.v2.ui.FormViewerActivity;
import com.trustudio.arts.v2.util.MyLocation.LocationResult;

public class FormBuilder {

	private final static String TAG = "#FB_";
	private final static String ERROR_MSG = "\n (Jawaban untuk pertanyaan ini bermasalah)";

	private FormViewerActivity mContext;
	private LayoutInflater inflater;
	private TimePickerDialog timeDialog;
	private DatePickerDialog dateDialog;

	private TextWatcher textWatcher;

	public List<View> listOfView = new ArrayList<View>();

	public FormBuilder(FormViewerActivity context) {
		this.mContext = context;
		inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	}

	public View createPhotoForm(JSONObject json) {
		View v = inflater.inflate(R.layout.answer_type_photo, null);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);
			String question = json.getString(QuestionCons.QUESTION);

			((TextView) v.findViewById(R.id.txtSoal)).setText(question);

			final ImageView img = (ImageView) v.findViewById(R.id.imgPhoto);
			img.setTag(varname);

			String answer = mContext.getData().getAnswer().getString(varname);
			Bitmap bmp = BitmapFactory.decodeFile(answer);

			if (bmp != null)
				img.setImageBitmap(bmp);

			img.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String[] items = { "Take From Camera", "Select From Gallery" };
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					builder.setItems(items, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int pos) {
							Intent intent = null;
							switch (pos) {
							case 0:
								intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
								intent.putExtra("outputX", 250);
								intent.putExtra("outputY", 250);
								intent.putExtra("return-data", true);
								break;
							case 1:
								intent = new Intent(Intent.ACTION_GET_CONTENT);
								intent.setType("image/*");
								intent.putExtra("outputX", 250);
								intent.putExtra("outputY", 250);
								intent.putExtra("return-data", true);
								break;
							}
							if (listOfView.size() > 0 && listOfView.contains(img)) {
								listOfView.remove(img);
							}
							int requestCode = Integer.parseInt(String.valueOf(RequestConstant.REQUEST_PHOTO)
									+ String.valueOf(listOfView.size()));
							mContext.startActivityForResult(intent, requestCode);
							listOfView.add(img);
							dialog.dismiss();
						}
					});
					builder.create().show();
				}
			});
		} catch (Exception e) {
			Log.e("createPhotoForm", e.toString());
		}
		return v;
	}

	public View createGpsLocationForm(JSONObject json) {
		View v = inflater.inflate(R.layout.answer_type_gps_loc, null);

		try {
			String question = json.getString(QuestionCons.QUESTION);
			TextView txt = (TextView) v.findViewById(R.id.txtSoal);
			txt.setText(question);

			final EditText inpLatitude = (EditText) v.findViewById(R.id.inpLat);
			final EditText inpLongitude = (EditText) v.findViewById(R.id.inpLong);
			Button btnGet = (Button) v.findViewById(R.id.btnGet);
			btnGet.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					MyLocation myLocation = new MyLocation(mContext);
					final ProgressDialog pd = new ProgressDialog(mContext);
					pd.setMessage("Fixing location...");
					pd.show();
					if (myLocation.cekProvider()) {
						myLocation.getLocation(new LocationResult() {

							@Override
							public void gotLocation(Location location) {
								if (location != null) {
									inpLongitude.setText(location.getLongitude() + "");
									inpLatitude.setText(location.getLatitude() + "");
								} else {
									inpLatitude.setText(R.string.text_note_available);
									inpLongitude.setText(R.string.text_note_available);
								}
								pd.dismiss();
							}
						});
					} else {
						Toast.makeText(mContext, "Please enable GPS", Toast.LENGTH_SHORT).show();
					}
				}
			});
		} catch (Exception e) {
			Log.e("create gps form", e.toString());
		}

		return v;
	}

	public View createAttachmentType(JSONObject json) {
		View v = inflater.inflate(R.layout.answer_type_attachment, null);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);
			String question = json.getString(QuestionCons.QUESTION);

			((TextView) v.findViewById(R.id.txtSoal)).setText(question);
			final EditText inpJawaban = (EditText) v.findViewById(R.id.inpJawaban);
			final ImageView status	  = (ImageView) v.findViewById(R.id.status);
			inpJawaban.setTag(varname);

			String answer = mContext.getData().getAnswer().getString(varname);
			inpJawaban.setText(answer);
			
			inpJawaban.addTextChangedListener(new TextWatcher() {
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if(count > 0){
						status.setImageResource(R.drawable.ic_status_ok);
					}
				}
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {}
				public void afterTextChanged(Editable s) {}
			});
			
			ImageButton btnAttach = (ImageButton) v.findViewById(R.id.btnAdd);
			btnAttach.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
					intent.setType("* / *");

					if (listOfView.size() > 0 && listOfView.contains(inpJawaban)) {
						listOfView.remove(inpJawaban);
					}
					int requestCode = Integer.parseInt(String.valueOf(RequestConstant.REQUEST_ATTACHMENT)
							+ String.valueOf(listOfView.size()));
					mContext.startActivityForResult(intent, requestCode);
					listOfView.add(inpJawaban);
				}
			});
		} catch (Exception e) {
			Log.e("createAttachment", e.toString());
		}
		return v;
	}

	public View createGroupTextType(JSONObject json) {
		final View parent = inflater.inflate(R.layout.answer_type_text_group, null);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);
			String question = json.getString(QuestionCons.QUESTION);

			((TextView) parent.findViewById(R.id.txtSoal)).setText(question);
			ImageButton btnAdd = (ImageButton) parent.findViewById(R.id.btnAdd);
			final EditText inpJawaban = (EditText) parent.findViewById(R.id.inpJawaban);
			final LinearLayout lyJawaban = (LinearLayout) parent.findViewById(R.id.lyJawaban);

			final TextWatcher groupWatcher = new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				}

				@Override
				public void afterTextChanged(Editable editable) {
					List<String> targetList = new ArrayList<String>();
					for (View v : listOfView) {
						if (v instanceof EditText) {
							if (v.getTag().toString().equals(inpJawaban.getTag())
									&& !((EditText) v).getText().toString().equals("")) {
								targetList.add(((EditText) v).getText().toString());
							}
						}
					}
					mContext.getData().inputArray(inpJawaban.getTag().toString(), targetList);
				}
			};

			/* set saved answer */
			JSONArray jarray = null;
			String answer = mContext.getData().getAnswer().getString(varname);
			if (answer != null && !answer.equals("")) {
				jarray = new JSONArray(answer);

				for (int i = 0; i < jarray.length(); i++) {
					EditText inp = addItemToGroup(inpJawaban, lyJawaban, groupWatcher);
					inp.setText(jarray.getString(i));
				}

			}

			inpJawaban.addTextChangedListener(groupWatcher);
			listOfView.add(inpJawaban);
			btnAdd.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					if (!inpJawaban.getText().toString().trim().equals("")) {
						addItemToGroup(inpJawaban, lyJawaban, groupWatcher);
					}
				}
			});
		} catch (Exception e) {
			Log.e("createGroupText", e.toString());
		}
		return parent;
	}

	private EditText addItemToGroup(final EditText inpJawaban, final LinearLayout lyJawaban,
			final TextWatcher groupWatcher) {
		final View child = inflater.inflate(R.layout.group_child, null);
		final EditText inp = (EditText) child.findViewById(R.id.inpChild);
		inp.setText(inpJawaban.getText().toString());
		inp.setTag(inpJawaban.getTag());
		((ImageButton) child.findViewById(R.id.btnDelete)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				lyJawaban.removeView(child);
				listOfView.remove(inp);

				List<String> targetList = new ArrayList<String>();
				for (View v : listOfView) {
					if (v instanceof EditText) {
						if (v.getTag().toString().equals(inpJawaban.getTag())) {
							targetList.add(((EditText) v).getText().toString());
						}
					}
				}
				mContext.getData().inputArray(inpJawaban.getTag().toString(), targetList);
			}
		});

		listOfView.add(inp);

		inp.addTextChangedListener(groupWatcher);
		inpJawaban.getText().clear();
		lyJawaban.addView(child);

		return inp;
	}
	
	/** Tambahan dari sigue
	 *  Status jawaban kalau sudah warna ijo, belum warna merah **/
	
	public View createTextType(JSONObject json) { // TODO
		View v = inflater.inflate(R.layout.answer_type_text, null);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);
			String question = json.getString(QuestionCons.QUESTION);
			String answer = json.getString(QuestionCons.ANSWER);
			
			//Debug.i("answer" + answer);
			
			((TextView) v.findViewById(R.id.txtSoal)).setText(question);
			final EditText inpJawaban = (EditText) v.findViewById(R.id.inpJawaban);
			final ImageView status	  = (ImageView) v.findViewById(R.id.status);
			
			ViewDataHolder holder = new ViewDataHolder();
			holder.setVarname(varname);
			holder.setViewType(QuestionCons.TYPE_TEXT);

			v.setTag(holder);
			inpJawaban.setTag(varname);
			
			if(answer.equals(""))
				inpJawaban.setText(mContext.getData().getAnswer().getString(varname));
			else
				inpJawaban.setText(answer);
			
			status.setImageResource(mContext.getData().getAnswer().getString(varname).equals("") 
					? R.drawable.ic_status_nok : R.drawable.ic_status_ok);
			
			inpJawaban.addTextChangedListener(textWatcher = new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
					if(arg3 > 0){
						status.setImageResource(R.drawable.ic_status_ok);
					}else{
						status.setImageResource(R.drawable.ic_status_nok);
					}
				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				}

				@Override
				public void afterTextChanged(Editable editable) {
					mContext.getData().inputData(inpJawaban.getTag().toString(), editable.toString().trim(),
							new OnAnswerTriggerListener() {

								@Override
								public void onVisibilitiyChange(String tag, boolean con) {
									/* Iterate list to find target not ideal */
									ViewDataHolder holder = null;
									for (View v : listOfView) {

										if (v.getTag() instanceof ViewDataHolder)
											holder = (ViewDataHolder) v.getTag();
										if (holder == null)
											return;

										if (holder.getVarname().equalsIgnoreCase(tag)) {
											if (con) {
												v.setVisibility(View.VISIBLE);
											} else if (v.getVisibility() == View.VISIBLE && !con) {
												disposeView(v, holder);
											}
										}
									}
								}

							});
				}
			});
		} catch (Exception e) {
			Log.e("createTextType", e.toString());
		}
		listOfView.add(v);
		return v;
	}

	public View createPasswordType(JSONObject json) {
		View v = createTextType(json);
		((EditText) v.findViewById(R.id.inpJawaban)).setTransformationMethod(PasswordTransformationMethod
				.getInstance());
		return v;
	}

	public View createIntegerType(JSONObject json) {
		View v = createTextType(json);
		((EditText) v.findViewById(R.id.inpJawaban)).setInputType(InputType.TYPE_CLASS_NUMBER
				| InputType.TYPE_NUMBER_FLAG_SIGNED);
		return v;
	}

	public View createDecimalType(JSONObject json) {
		View v = createTextType(json);
		((EditText) v.findViewById(R.id.inpJawaban)).setInputType(InputType.TYPE_CLASS_NUMBER
				| InputType.TYPE_NUMBER_FLAG_DECIMAL);
		return v;
	}

	public View createQRCodeType(JSONObject json) {
		View v = inflater.inflate(R.layout.answer_type_code_scan, null);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);
			String question = json.getString(QuestionCons.QUESTION);

			/* get question saved anser */
			String answer = mContext.getData().getAnswer().getString(varname);

			((TextView) v.findViewById(R.id.txtSoal)).setText(json.getString(question));
			final EditText inpJawaban = (EditText) v.findViewById(R.id.inpJawaban);
			inpJawaban.setTag(varname);
			inpJawaban.setText(answer);

			Button btnScan = (Button) v.findViewById(R.id.btnGet);
			btnScan.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent("com.google.zxing.client.android.SCAN");
					int requestCode = Integer.parseInt(String.valueOf(RequestConstant.REQUEST_CODE_SCAN)
							+ String.valueOf(listOfView.size()));
					listOfView.add(inpJawaban);
					mContext.startActivityForResult(intent, requestCode);
				}
			});
		} catch (Exception e) {
			Log.e("createQRCode", e.toString());
		}
		return v;
	}

	public View createTimeType(JSONObject json) {
		View v = createTextType(json);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);

			final EditText inpJawaban = (EditText) v.findViewById(R.id.inpJawaban);
			inpJawaban.setFocusable(false);
			inpJawaban.setLongClickable(false);
			inpJawaban.setTag(json.getString(QuestionCons.VARIABLE));

			/* Set saved anser */
			String answer = mContext.getData().getAnswer().getString(varname);
			if (answer != null && !answer.equals("")) {

				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(Long.valueOf(answer)));

				inpJawaban.setText(OpenDateTime.getTimeString(cal.get(Calendar.HOUR_OF_DAY)) + ":"
						+ OpenDateTime.getTimeString(cal.get(Calendar.MINUTE)));
			}

			/** Tambahan Sigue **/
			inpJawaban.addTextChangedListener(this.textWatcher);
			
			inpJawaban.removeTextChangedListener(this.textWatcher);
			inpJawaban.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					timeDialog = new TimePickerDialog(mContext, new OnTimeSetListener() {

						@Override
						public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
							inpJawaban.setText(OpenDateTime.getTimeString(hourOfDay) + ":"
									+ OpenDateTime.getTimeString(minute));
							mContext.getData().inputData(inpJawaban.getTag().toString(),
									inpJawaban.getText().toString());
						}
					}, OpenDateTime.getCurrentHour(), OpenDateTime.getCurrentMinute(), true);
					timeDialog.show();
				}
			});
		} catch (Exception e) {
			Log.e("createTimeType", e.toString());
		}
		return v;
	}

	public View createDateTimeType(JSONObject json) {
		View v = createTextType(json);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);

			final EditText inpJawaban = (EditText) v.findViewById(R.id.inpJawaban);
			inpJawaban.setFocusable(false);
			inpJawaban.setLongClickable(false);
			inpJawaban.setTag(json.getString(QuestionCons.VARIABLE));

			/* Set saved anser */
			String answer = mContext.getData().getAnswer().getString(varname);
			if (answer != null && !answer.equals("")) {

				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(Long.valueOf(answer)));

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

				final StringBuilder sb = new StringBuilder();
				sb.append(OpenDateTime.getIndonesianDay(cal.get(Calendar.DAY_OF_WEEK)) + ", "
						+ sdf.format(cal.getTime()) + " "
						+ OpenDateTime.getTimeString(cal.get(Calendar.HOUR_OF_DAY)) + ":"
						+ OpenDateTime.getTimeString(cal.get(Calendar.MINUTE)));
				inpJawaban.setText(sb.toString());
			}

			inpJawaban.removeTextChangedListener(this.textWatcher);
			inpJawaban.addTextChangedListener(this.textWatcher);
			inpJawaban.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Calendar now = Calendar.getInstance();
					dateDialog = new DatePickerDialog(mContext, new OnDateSetListener() {

						@Override
						public void onDateSet(DatePicker arg0, int year, int month, int day) {
							final Calendar cal = OpenDateTime.getCalendar(year, month, day);
							SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
							final StringBuilder sb = new StringBuilder();
							sb.append(OpenDateTime.getIndonesianDay(cal.get(Calendar.DAY_OF_WEEK)) + ", "
									+ sdf.format(cal.getTime()));
							timeDialog = new TimePickerDialog(mContext, new OnTimeSetListener() {

								@Override
								public void onTimeSet(TimePicker arg0, int hourOfDay, int minute) {
									sb.append(" " + OpenDateTime.getTimeString(hourOfDay) + ":"
											+ OpenDateTime.getTimeString(minute));
									inpJawaban.setText(sb.toString());
									cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
									cal.set(Calendar.MINUTE, minute);
									mContext.getData().inputData(inpJawaban.getTag().toString(),
											cal.getTime().toString());
								}
							}, OpenDateTime.getCurrentHour(), OpenDateTime.getCurrentMinute(), true);
							timeDialog.show();
						}
					}, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
					dateDialog.show();
				}
			});
		} catch (Exception e) {
			Log.e("createTimeType", e.toString());
		}
		return v;
	}

	public View createDateType(JSONObject json) {
		View v = createTextType(json);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);

			final EditText inpJawaban = (EditText) v.findViewById(R.id.inpJawaban);
			inpJawaban.setFocusable(false);
			inpJawaban.setLongClickable(false);
			inpJawaban.setTag(varname);

			/* Set saved anser */
			String answer = mContext.getData().getAnswer().getString(varname);
			if (answer != null && !answer.equals("")) {

				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(Long.valueOf(answer)));

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				inpJawaban.setText(OpenDateTime.getIndonesianDay(cal.get(Calendar.DAY_OF_WEEK)) + ", "
						+ sdf.format(cal.getTime()));
			}
			
			/** Tambahan Sigue **/
			inpJawaban.addTextChangedListener(this.textWatcher);
			
			inpJawaban.removeTextChangedListener(this.textWatcher);
			inpJawaban.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Calendar now = Calendar.getInstance();
					dateDialog = new DatePickerDialog(mContext, new OnDateSetListener() {

						@Override
						public void onDateSet(DatePicker arg0, int year, int month, int day) {
							Calendar cal = OpenDateTime.getCalendar(year, month, day);
							SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
							inpJawaban.setText(OpenDateTime.getIndonesianDay(cal.get(Calendar.DAY_OF_WEEK))
									+ ", " + sdf.format(cal.getTime()));
							mContext.getData().inputData(inpJawaban.getTag().toString(),
									cal.getTime().toString());
						}
					}, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
					dateDialog.show();
				}
			});
		} catch (Exception e) {
			Log.e("createTimeType", e.toString());
		}
		return v;
	}

	public View createSignatureType(JSONObject json) {
		View v = inflater.inflate(R.layout.answer_type_signature, null);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);
			String question = json.getString(QuestionCons.QUESTION);

			((TextView) v.findViewById(R.id.txtSoal)).setText(json.getString(question));

			final ImageView img = (ImageView) v.findViewById(R.id.imgPhoto);
			img.setTag(varname);

			String answer = mContext.getData().getAnswer().getString(varname);
			Bitmap bmp = BitmapFactory.decodeFile(answer);
			if (bmp != null)
				img.setImageBitmap(bmp);

			RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.lyJawaban);
			rl.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(mContext, FingerPaint.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					if (listOfView.contains(img)) {
						listOfView.remove(img);
					}
					int requestCode = Integer.parseInt(String.valueOf(RequestConstant.REQUEST_SIGNATURE)
							+ String.valueOf(listOfView.size()));
					mContext.startActivityForResult(intent, requestCode);
					listOfView.add(img);
				}
			});
		} catch (Exception e) {
			Log.e("createSignatureType", e.toString());
		}
		return v;
	}

	private View createFixedChoice(JSONObject json, String[] item, int type, OnClickListener onClickListener) {
		View v = createRadioQuestion(json, null);
		try {

			String varname = json.getString(QuestionCons.VARIABLE);
			String question = json.getString(QuestionCons.QUESTION);

			((TextView) v.findViewById(R.id.txtSoal)).setText(question);
			final ImageView status	  = (ImageView) v.findViewById(R.id.status);
			
			RadioButton radio;
			ViewDataHolder holder = new ViewDataHolder();
			holder.setVarname(varname);
			holder.setViewType(type);

			v.setTag(holder);

			String answer = mContext.getData().getAnswer().getString(varname);
			JSONObject savedAnswer = null;
			if (!answer.equals("")) {
				savedAnswer = new JSONObject(answer);
			}
			
			Log.d("savedAnser", answer + "| this is savedAnswer");

			LinearLayout lyJawaban = (LinearLayout) v.findViewById(R.id.lyJawaban);
			for (int i = 0; i < item.length; i++) {

				String childVarName = QuestionData.getVarname(varname, i);

				radio = getRadio(item[i], json.getString(QuestionCons.VARIABLE), onClickListener);
				radio.setTag(childVarName);
				radio.setId(i);
				lyJawaban.addView(radio);

				/* fill choice with answer if any */
				if (savedAnswer != null) {
					if (savedAnswer.has(childVarName)) {
						if (savedAnswer.getBoolean(childVarName))
							radio.setChecked(true);
					}
				}

				if(radio.isChecked()) status.setImageResource(R.drawable.ic_status_ok);
				
				radio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(isChecked)
							status.setImageResource(R.drawable.ic_status_ok);
					}
				});
			}
		} catch (Exception e) {
			Log.e("createFixedChoice", e.toString());
		}

		return v;
	}

	public View createYesNoType(JSONObject json, OnClickListener onClickListener) {
		String[] items = { mContext.getString(R.string.yes), mContext.getString(R.string.no) };
		return createFixedChoice(json, items, QuestionCons.TYPE_YES_NO, onClickListener);
	}

	public View createOkNokType(JSONObject json, OnClickListener onClickListener) {
		String[] items = { mContext.getString(R.string.ok), mContext.getString(R.string.not_ok) };
		return createFixedChoice(json, items, QuestionCons.TYPE_OK_NOK, onClickListener);
	}

	public View createOkNokNaType(JSONObject json, OnClickListener onClickListener) {
		String[] items = { mContext.getString(R.string.ok), mContext.getString(R.string.not_ok),
				mContext.getString(R.string.not_available) };
		return createFixedChoice(json, items, QuestionCons.TYPE_OK_NOK_NA, onClickListener);
	}

	private View createRadioQuestion(JSONObject json, OnClickListener onClickListener) {
		View v = inflater.inflate(R.layout.answer_type_choice, null);
		try {
			String question = json.getString(QuestionCons.QUESTION);
			((TextView) v.findViewById(R.id.txtSoal)).setText(question);
		} catch (Exception e) {
			Log.e("createRadioQuestion", e.toString());
		}
		return v;
	}

	public View createMultiSelectType(JSONObject json) {
		View v = inflater.inflate(R.layout.answer_type_check, null);
		try {
			String varname = json.getString(QuestionCons.VARIABLE);
			String question = json.getString(QuestionCons.QUESTION);

			ViewDataHolder holder = new ViewDataHolder();
			holder.setVarname(varname);
			holder.setViewType(QuestionCons.TYPE_MULTI_SELECT);

			JSONArray jarray = null;
			String answer = mContext.getData().getAnswer().getString(varname);
			if (answer != null && !answer.equals("")) {
				jarray = new JSONArray(answer);
			}

			v.setTag(holder);
			
			final ImageView status = (ImageView) v.findViewById(R.id.status);
			LinearLayout lyJawaban = (LinearLayout) v.findViewById(R.id.lyJawaban);
			((TextView) v.findViewById(R.id.txtSoal)).setText(question);
			
			/** Tambahan siGue, bikin onCheckedlisener baru
			 * dan menghapus listener pada yang di include pada method createMultiSelectType**/
			
			OnCheckedChangeListener listeners = new OnCheckedChangeListener() {
				public void onCheckedChanged(CompoundButton cb, boolean isChecked) {
					List<String> targetList = new ArrayList<String>();
					ViewGroup lyJawaban = (ViewGroup) cb.getParent();
					
					int check = 0;
					
					for (int i = 0; i < lyJawaban.getChildCount(); i++) {
						cb = (CompoundButton) lyJawaban.getChildAt(i);
						if (cb.isChecked()) {
							targetList.add(cb.getText().toString());
						}else{
							check++;
							if(check == lyJawaban.getChildCount()){
								status.setImageResource(R.drawable.ic_status_nok);
							}
						}
					}
					
					if(isChecked) status.setImageResource(R.drawable.ic_status_ok);
					
					mContext.getData().inputArray(cb.getTag().toString(), targetList);
					String answer;
					try {
						answer = mContext.getData().getAnswer().getString(cb.getTag().toString());
						Log.d("answer", answer);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
				}
			};
			
			JSONArray jawaban = json.getJSONArray(QuestionCons.OPT_VAL);
			for (int i = 0; i < jawaban.length(); i++) {

				boolean isCheked = false;
				if (jarray != null) {
					for (int j = 0; j < jarray.length(); j++) {
						String savedAnswer = jarray.getString(j);
						if (savedAnswer.equalsIgnoreCase(jawaban.getString(i)))
							isCheked = true;
					}
				}
				
				if(isCheked) status.setImageResource(R.drawable.ic_status_ok);
				
				View cekButton = getCheckItem(jawaban.getString(i), varname, listeners, isCheked);
				//cekButton.setTag(QuestionData.getVarname(varname, i));
				cekButton.setId(i);

				lyJawaban.addView(cekButton);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("createMultiSelecet", e.toString());
		}
		return v;
	}

	private View getCheckItem(String value, String tag, OnCheckedChangeListener listener, boolean isChecked) {
		CheckBox cb = new CheckBox(mContext);
		cb.setChecked(isChecked);
		cb.setTag(tag);
		cb.setText(value);
		cb.setCompoundDrawablePadding(10);
		cb.setOnCheckedChangeListener(listener);
		listOfView.add(cb);
		return cb;
	}

	public View createMultipleChoiceType(JSONObject json, OnClickListener onClickListener) {
		View v = inflater.inflate(R.layout.answer_type_choice, null);
		try {

			String varname = json.getString(QuestionCons.VARIABLE);
			String question = json.getString(QuestionCons.QUESTION);
			
			final ImageView status	  = (ImageView) v.findViewById(R.id.status);
			TextView txtQuestion = (TextView) v.findViewById(R.id.txtSoal);
			txtQuestion.setText(question);

			RadioGroup lyJawaban = (RadioGroup) v.findViewById(R.id.lyJawaban);

			RadioButton radio;
			ViewDataHolder holder = new ViewDataHolder();
			holder.setVarname(varname);
			holder.setViewType(QuestionCons.TYPE_MULTI_RADIO);

			v.setTag(holder);

			/* get saved answer for multiple choice */
			String answer = mContext.getData().getAnswer().getString(varname);
			JSONObject savedAnswer = null;
			if (!answer.equals("")) {
				savedAnswer = new JSONObject(answer);
			}

			Log.d("varname", varname);
			JSONArray jawaban = null;
			try {
				jawaban = json.getJSONArray(QuestionCons.OPT_VAL);
			} catch (JSONException e) {
				txtQuestion.setText(txtQuestion.getText().toString() + ERROR_MSG);
				return v;
			}

			for (int i = 0; i < jawaban.length(); i++) {

				radio = getRadio(String.valueOf(jawaban.getString(i)), json.getString(QuestionCons.VARIABLE),
						onClickListener);

				String childVarName = QuestionData.getVarname(varname, i);
				radio.setTag(childVarName);
				radio.setId(i);

				/* fill choice with answer if any */
				if (savedAnswer != null) {
					if (savedAnswer.has(childVarName)) {
						if (savedAnswer.getBoolean(childVarName))
							radio.setChecked(true);
					}
				}

				if(radio.isChecked()) status.setImageResource(R.drawable.ic_status_ok);
				
				radio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if(isChecked)
							status.setImageResource(R.drawable.ic_status_ok);
					}
				});
				lyJawaban.addView(radio);
				holder.setAnswerCount(i + 1);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("createMultipleChoice", e.toString());
		}

		listOfView.add(v);
		return v;
	}

	private RadioButton getRadio(String answer, String tag, OnClickListener ocl) {
		RadioButton radio = new RadioButton(mContext);
		radio.setText(answer);
		radio.setCompoundDrawablePadding(10);
		radio.setOnClickListener(ocl);
		radio.setTag(tag);
		return radio;
	}

	/** Return container berupa linear layout untuk form question */
	public LinearLayout getContainer() {
		LinearLayout llVertical = (LinearLayout) inflater.inflate(R.layout.form_page_container, null);
		return llVertical;
	}

	/** Return scrollcontainer untuk container form question */
	public ScrollView getScrollCountainer() {
		ScrollView sv = (ScrollView) inflater.inflate(R.layout.form_page_scroll_container, null);
		return sv;
	}

	public View getSubmitForm() {
		View v = inflater.inflate(R.layout.submit_form, null);
		return v;
	}

	public OnClickListener onClickSave = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (v instanceof RadioButton) {
				RadioButton radio = (RadioButton) v;
				mContext.getData().inputData(v.getTag().toString(), radio.getText().toString());
			}
		}
	};
	
	public OnCheckedChangeListener onChekedSave = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(CompoundButton cb, boolean isChecked) {
			List<String> targetList = new ArrayList<String>();
			ViewGroup lyJawaban = (ViewGroup) cb.getParent();
			
			for (int i = 0; i < lyJawaban.getChildCount(); i++) {
				cb = (CompoundButton) lyJawaban.getChildAt(i);
				if (cb.isChecked()) {
					targetList.add(cb.getText().toString());
				}
			}

			mContext.getData().inputArray(cb.getTag().toString(), targetList);
			String answer;
			try {
				answer = mContext.getData().getAnswer().getString(cb.getTag().toString());
				Log.d("answer", answer);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	};

	/* Radio Button Click Listener */
	public OnClickListener ocl = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (v instanceof RadioButton) {

				RadioGroup container = (RadioGroup) v.getParent();
				ViewDataHolder holder = (ViewDataHolder) ((View) container.getParent()).getTag();

				try {

					JSONObject json = new JSONObject();
					for (int i = 0; i < container.getChildCount(); i++) {

						String varName = container.getChildAt(i).getTag().toString();

						if (container.getChildAt(i) == v) {
							json.put(varName, String.valueOf(true));
						} else {
							json.put(varName, String.valueOf(false));
						}
					}

					mContext.getData().inputData(holder.getVarname(), json, new OnAnswerTriggerListener() {

						@Override
						public void onVisibilitiyChange(String tag, boolean con) {

							/* Iterate list to find target not ideal */
							ViewDataHolder holder = null;
							for (View v : listOfView) {

								if (v.getTag() instanceof ViewDataHolder)
									holder = (ViewDataHolder) v.getTag();
								if (holder == null)
									return;

								if (holder.getVarname().equalsIgnoreCase(tag)) {
									if (con) {
										v.setVisibility(View.VISIBLE);
									} else if (v.getVisibility() == View.VISIBLE && !con) {
										disposeView(v, holder);
									}
								}
							}

						}
					});

				} catch (Exception e) {
					Log.e(TAG + "onRadioClick", e.toString());
				}
			}
		}
	};

	private void disposeView(View v, ViewDataHolder holder) {

		if (holder == null) {
			v.setVisibility(View.GONE); // TODO handle other than text and multi
										// radio
			return;
		}

		if (holder.getViewType() == QuestionCons.TYPE_TEXT) {
			EditText inp = (EditText) v.findViewById(R.id.inpJawaban);
			if (inp != null) {
				inp.setText("");
			}
			v.setVisibility(View.GONE);
		}

		if (holder.getViewType() == QuestionCons.TYPE_MULTI_RADIO) {
			RadioGroup lyJawaban = (RadioGroup) v.findViewById(R.id.lyJawaban);

			RadioButton radio;
			for (int j = 0; j < lyJawaban.getChildCount(); j++) {
				radio = (RadioButton) lyJawaban.getChildAt(j);
				radio.setChecked(false);
			}
			v.setVisibility(View.GONE);
		}

	}
}
