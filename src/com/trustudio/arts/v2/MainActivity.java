package com.trustudio.arts.v2;

import com.trustudio.arts.v2.ui.BaseActivity;

import android.os.Bundle;

public class MainActivity extends BaseActivity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

}
