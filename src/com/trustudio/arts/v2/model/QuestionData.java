package com.trustudio.arts.v2.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.trustudio.arts.v2.util.ConditionParser;
import com.trustudio.arts.v2.util.OpenData;
import com.trustudio.arts.v2.util.QuestionCons;

import android.content.ContextWrapper;
import android.util.Log;

/** Class yang mengurus dan berfungsi sebagai helper Question **/

public class QuestionData {

	public JSONObject answer = new JSONObject();
	public JSONObject condition = new JSONObject();

	/*
	 * JSONObject that has varname of triggered variable extract from json
	 * TriggerTo
	 */
	public JSONObject trigger = new JSONObject();

	private ContextWrapper mContext;

	public QuestionData(ContextWrapper context) {
		mContext = context;
	}

	public JSONArray getQuestionAtId(int templateDocId) {
		JSONArray jarray = null;
		try {

			// JSONObject json = DummyQuestionData.getAllData();

			JSONObject json = new JSONObject(OpenData.getQuestionData(mContext, templateDocId));

			Log.d("QuestionJSON", json.toString());

			jarray = json.getJSONArray("data");

		} catch (Exception e) {
			Log.e("getQuestionAtId", e.toString());
		}
		return jarray;
	}

	/* Input Data with Trigger Callback for String type answer */
	public void inputData(String iden, String jawaban, OnAnswerTriggerListener listener) {
		inputData(iden, jawaban);
		triggerCheck(iden, listener);
	}

	/* Input Data with Trigger Callback for JSONObject type answer */
	public void inputData(String iden, JSONObject jawaban, OnAnswerTriggerListener listener) {
		inputData(iden, jawaban);
		triggerCheck(iden, listener);
	}

	/* Check if answer has trigger something */
	private void triggerCheck(String iden, OnAnswerTriggerListener listener) {
		if (trigger.has(iden)) {

			/**
			 * Get All Target ID and loop for the number of target \\ convert |
			 * to plain text
			 */

			try {
				String[] targetID = trigger.getString(iden).split("\\|");
				for (int i = 0; i < targetID.length; i++) {
					Log.d("con target", targetID[i]);
					/** Parser will handle ALL the shit and return TRUE or FALSE */

					boolean con = false;
					if (condition.get(targetID[i]) instanceof String) {
						JSONObject json = new JSONObject();
						json.put(QuestionCons.CONDITION, condition.getString(targetID[i]));

						con = ConditionParser.parse(json, answer);

					} else {
						con = ConditionParser.parse(condition.getJSONObject(targetID[i]), answer);
					}

					/** Callback */
					listener.onVisibilitiyChange(targetID[i], con);

					Log.d("con hasilnya", con + "");
					// Log.e("condition", condition.toString());
					// Log.e("list", trigger.toString());
				}
			} catch (JSONException e) {
				Log.e("condition", condition.toString());
				Log.e("error", e.toString());
				Log.e("list", trigger.toString());
			}
		}
	}

	public void inputCondition(String iden, JSONObject value) {
		try {
			condition.put(iden, value);
		} catch (JSONException e) {
			Log.e("input condition", e.toString());
		}
	}

	public void inputCondition(String iden, String value) {
		try {
			condition.put(iden, value);
		} catch (JSONException e) {
			Log.e("input condition", e.toString());
		}
	}

	public void inputData(String iden, String jawaban) {
		try {
			answer.put(iden, jawaban);
		} catch (JSONException e) {
			Log.e("input data", e.toString());
		}
	}

	public void inputData(String iden, JSONObject jawaban) {
		try {
			answer.put(iden, jawaban);
		} catch (JSONException e) {
			Log.e("input data", e.toString());
		}
	}

	public void inputArray(String iden, List<String> listJawaban) {
		try {
			answer.put(iden, new JSONArray(listJawaban));
			listJawaban = null;
		} catch (Exception e) {
			Log.e("input array", e.toString());
		}
	}

	public JSONObject getAnswer() {
		return answer;
	}

	public String getAnswer(String iden) {
		String check = null;
		
		try {
			if (answer.get(iden).equals("")) {
				check = "null";
			}else{
				check = "ok";
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return check;
	}

	public void registerTrigger(String var, String value) {
		if (!trigger.has(var)) {
			try {
				trigger.put(var, value);
			} catch (JSONException e) {
				Log.e("register trigger", e.toString());
			}
		}
	}

	public static String getVarname(String var, int pos) {
		return var + "_" + (pos + 1);
	}

	public interface OnAnswerTriggerListener {
		public void onVisibilitiyChange(String tag, boolean con);
	}
}
